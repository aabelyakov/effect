#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 12.04.2013
# Purpose: Контроллер "по умолчанию"
#############################################################################
#               Effect - Расчёт эффективности грузоперевозок
#                         (С) А.А.Беляков 2013
#############################################################################
__revision__ = "$Id$"
#############################################################################
def index():
    """
    ИФК. Начальная страница приложения
    """
    #------------------------------------------------------------------------
    return dict(
        message = 'НАЧАЛЬНАЯ СТРАНИЦА',
        sts1 = carrier_snaim()
    )
#enddef
#############################################################################
def user():
    """
    Функция вызывается при обращении пользователя по следующим URL:
    - http://.../[app]/default/user/login
    - http://.../[app]/default/user/logout
    - http://.../[app]/default/user/register
    - http://.../[app]/default/user/profile
    - http://.../[app]/default/user/retrieve_password
    - http://.../[app]/default/user/change_password
    -------------------------------------------------------------------------
    Используйте указанные декораторы для функций, которые нуждаются
    в контроле доступа:
    - @auth.requires_login()
    - @auth.requires_membership('group name')
    - @auth.requires_permission('read', 'table name', record_id)
    """
    #------------------------------------------------------------------------
    # Вход только для авторизованных пользователей
    return dict(form=auth()) 
#enddef
#############################################################################
def download():
    """
    Функция вызывается при обращении пользователя по следующему URL:
    http://.../[app]/default/download/[filename]
    """
    #------------------------------------------------------------------------
    return response.download(request, db)
#enddef
#############################################################################
def call():
    """
    Функция активизирует сервисы xml, json, xmlrpc, jsonrpc, amfrpc,
    rss, csv
    -------------------------------------------------------------------------
    При обращении пользователя по URL:
    http://..../[app]/default/call/jsonrpc
    будет вызван сервис JSON-RPC
    -------------------------------------------------------------------------
    Используйте декоратор @service.jsonrpc перед функцией, которая будет
    предоставлять сервис JSON-RPC
    """
    #------------------------------------------------------------------------
    return service()
#enddef
#############################################################################
