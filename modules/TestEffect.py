# !/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 24.07.2012
# Purpose: Проверка работы БД effect.sqlite из приложения effect
# Внимание! Запускать ссылку Z_TestDbEffect.py из каталога web2py       
#############################################################################
#               Effect - Расчёт эффективности грузоперевозок
#                         (С) А.А.Беляков 2013
#############################################################################
__revision__ = "$Id: TestDbEffect.py 224 2013-06-02 15:45:08Z aabelyakov $"
#############################################################################
import sys
import os  
#----------------------------------------------------------------------------
# Переключение текущего рабочего каталога - на каталог web2py
os.chdir('/home/aab/MyPython/web2py')
#print(os.getcwd())
#/home/aab/MyPython/web2py
#----------------------------------------------------------------------------
# Добавление пути к web2py в список sys.path путей поиска python-модулей
sys.path.append(os.getcwd())
#----------------------------------------------------------------------------
from gluon import *
#from gluon.dal import DAL, Field
#from gluon.sqlhtml import SQLTABLE 
#----------------------------------------------------------------------------
# Импортируем db
from applications.effect.models.e1_db import db
from applications.effect.models.e3_lib import *
#############################################################################
'''
print db.tables
# ['adr', 'firm', 'pers', 'gdman', 'car', 'fuel', 'trail', 'taxip',
# 'taxfl', 'item', 'wage', 'good', 'elem', 'oreis', 'odxrx']
#----------------------------------------------------------------------------
print db._uri
# ('sqlite://effect.sqlite',)
#----------------------------------------------------------------------------
print type(db)
sets = db(db.adr.snaim=="Н.Новгород")
print type(sets)
#----------------------------------------------------------------------------
# Наследование полей и таблиц

# Отдельное определение поля name
nam = Field('name')

# Таблицы a, b и c создаётся не в БД, а в оперативной паямяти,
# т.к. migrate=False
db.define_table('a', nam, Field("bob"), migrate=False) 
db.define_table('b', Field('other'), Field("rob"), migrate=False) 
db.define_table('c', Field('test'), db.a, db.b, migrate=False)

print type(db.c)
# <class 'gluon.dal.Table'>

print db.c.fields 
# ['id', 'test', 'name', 'bob', 'other', 'rob']

flt = db.firm.id==2
print db(flt).select()
print db().select(db.firm.snaim)
#----------------------------------------------------------------------------
for field in db.oreis:
    print field
#endfor

"""
oreis.id
oreis.firm1_id
oreis.pers_id
oreis.car_id
oreis.trail_id
oreis.firm2_id
oreis.sgdout_id
oreis.good_id
oreis.firm3_id
oreis.sgdin_id
oreis.route
oreis.date
oreis.num
oreis.dtb
oreis.dte
oreis.dlina
oreis.dlit
oreis.dx
oreis.rx
"""
#----------------------------------------------------------------------------
def myfunc(*args): 
    print args
    global ss
    ss = args
#endif
#----------------------------------------------------------------------------
db.odxrx._after_insert.append(lambda f, id: myfunc(f,id))
db.odxrx._after_update.append(lambda s, f: myfunc(s,f))
db.odxrx._after_delete.append(lambda s: myfunc(s))


#db(db.odxrx.id==8).delete()
db(db.odxrx.id==1).update(summa=3)
#db.odxrx.insert(summa=33.56)
db.commit()

print "ss =", ss
print "ss[0] =", ss[0]
print type(ss[0])
print type((ss[0].query))
print "ss[1] =", ss[1]
print ss[1]["summa"]
for k, v in ss[1].items():
    print k, v
#endfor  
#--------------------------------------------------------------------
oreis_id=1
#--------------------------------------------------------------------
sumdx = db.odxrx.summa.sum()
#--------------------------------------------------------------------
# Выборка из таблицы odxrx ДОХОДЫ И РАСХОДЫ записи с итоговой суммой
# доходов dx по колонке summa (от 00 до 09)
row = db(db.odxrx.oreis_id==oreis_id) \
  (db.elem.id==db.odxrx.elem_id) \
  (db.elem.numst=="00").select(sumdx).first()
#--------------------------------------------------------------------
# Итоговая сумма доходов dx 
dx = row[sumdx]
#--------------------------------------------------------------------
sumrx = db.odxrx.summa.sum()
#--------------------------------------------------------------------
# Выборка из таблицы odxrx ДОХОДЫ И РАСХОДЫ записи с итоговой суммой
# расходов rx по колонке summa (от 50 до 59)
row = db(db.odxrx.oreis_id==oreis_id) \
  (db.elem.id==db.odxrx.elem_id) \
  ((db.elem.numst=="50") | (db.elem.numst=="51")).select(sumrx).first()
#--------------------------------------------------------------------
# Итоговая сумма расходов rx 
rx = row[sumrx]
print rx
#----------------------------------------------------------------------------
'''
#############################################################################
print db.wage