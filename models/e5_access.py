#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 12.04.2013
# Purpose: Модель - авторизация доступа
#############################################################################
#               Effect - Расчёт эффективности грузоперевозок
#                         (С) А.А.Беляков 2013
#############################################################################
__revision__ = "$Id$"
#############################################################################
if db(db.auth_user.id).select():
    # Таблица auth_user (ПОЛЬЗОВАТЕЛИ) не пуста
    #------------------------------------------------------------------------
    if not session.bel_id:
        # Нахождение записи о пользователе Белякове А.А. в таблице auth_user
        # (ПОЛЬЗОВАТЕЛИ)
        row = db(db.auth_user.last_name == 'Belyakov') \
            .select(db.auth_user.id).first()
        #--------------------------------------------------------------------
        # Сохранение идентификатора записи о пользователе Белякове А.А.в
        # сессионной переменной
        session.bel_id = row.id
    #endif
    #------------------------------------------------------------------------
    if not session.koz_id:
        # Нахождение записи о пользователе Кожине А.А. в таблице auth_user
        # (ПОЛЬЗОВАТЕЛИ)
        row = db(db.auth_user.last_name == 'Kozhin')\
            .select(db.auth_user.id).first()
        #--------------------------------------------------------------------
        # Сохранение идентификатора записи о пользователе Кожине А.А. в
        # сессионной переменной
        session.koz_id = row.id
    #endif
    #------------------------------------------------------------------------
else:
    # Таблица auth_user (ПОЛЬЗОВАТЕЛИ) пуста
    #------------------------------------------------------------------------
    # Чистка таблицы auth_user (функция из библиотеки-модели lib.py)
    TruncTable('auth_user')
    #------------------------------------------------------------------------
    # Вставка новой записи в таблицу auth_user (ПОЛЬЗОВАТЕЛИ) с данными
    # пользователя - Белякова А.А.
    session.bel_id = db.auth_user.insert(
        first_name = 'Anatoly',
        last_name = 'Belyakov',
        email = 'aabelyakov@mail.ru',
        password = db.auth_user.password.requires[0]('effect')[0],
        registration_key = ''
    )
    #------------------------------------------------------------------------
    # Вставка новой записи в таблицу auth_user (ПОЛЬЗОВАТЕЛИ) с данными
    # пользователя -  Кожина А.А.
    session.koz_id = db.auth_user.insert(
        first_name = 'Alex',
        last_name = 'Kozhin',
        email = 'aka00@list.ru',
        password = db.auth_user.password.requires[0]('qwerty')[0],
        registration_key = ''
    )
#endif
#============================================================================
if db(db.auth_group.id).select():
    # Таблица auth_group (ГРУППЫ ПОЛЬЗОВАТЕЛЕЙ) не пуста
    #------------------------------------------------------------------------
    if not session.adm_id:
        # Нахождение записи о группе admin (АДМИНИСТРАТОРЫ) в таблице
        # auth_group (ГРУППЫ)
        row = db(db.auth_group.role == 'admin') \
            .select(db.auth_group.id).first()
        #--------------------------------------------------------------------
        # Сохранение идентификатора записи с группой admin в сессионной
        # переменной
        session.adm_id = row.id
    #endif
    #------------------------------------------------------------------------
    if not session.usr_id:
        # Нахождение записи о группе user (ПОЛЬЗОВАТЕЛИ) таблице auth_group
        # (ГРУППЫ)
        row = db(db.auth_group.role == 'user') \
            .select(db.auth_group.id).first()
        #--------------------------------------------------------------------
        # Сохранение идентификатора записи с группой user в сессионной
        # переменной
        session.usr_id = row.id
    #endif
    #------------------------------------------------------------------------
else:
    # Таблица auth_group (ГРУППЫ ПОЛЬЗОВАТЕЛЕЙ) пуста
    #------------------------------------------------------------------------
    # Чистка таблицы auth_group (функция из библиотеки-модели lib.py)
    TruncTable('auth_group')
    #------------------------------------------------------------------------
    # Создание записи о группе admin - 'Администраторы приложения'
    session.adm_id = auth.add_group(
        'admin',
        'Администраторы приложения'
    )
    #------------------------------------------------------------------------
    # Создание записи о группе user - 'Пользователи приложения'
    session.usr_id = auth.add_group(
        'user',
        'Пользователи приложения'
    )
#endif
#----------------------------------------------------------------------------
if not db(db.auth_membership.id).select():
    # Таблица auth_membership (ГРУППЫ - ПОЛЬЗОВАТЕЛИ) пуста
    #------------------------------------------------------------------------
    # Чистка таблицы auth_user (функция из библиотеки-модели lib.py)
    TruncTable('auth_membership')
    #------------------------------------------------------------------------
    # Пользователь session.bel_id включен в группу session.adm_id
    auth.add_membership(session.adm_id, session.bel_id)
    #------------------------------------------------------------------------
    # Пользователь session.koz_id включен в группу session.adm_id
    auth.add_membership(session.adm_id, session.koz_id)
#endif
#############################################################################