#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
# Created: 12.04.2013
# Purpose: Библиотека функций для контроллеров
#############################################################################
#               Effect - Расчёт эффективности грузоперевозок
#                         (С) А.А.Беляков 2013
#############################################################################
# Глобальный справочник наименований таблиц
dTabl= {
    "oreis": "РЕЙСЫ",
    "odxrx": "ДОХОДЫ и РАСХОДЫ",
    "adr": "АДРЕСА",
    "car": "АВТОМОБИЛИ",
    "fuel": "ТОПЛИВО",
    "gdman": "ГРУЗООБРАБОТЧИКИ",
    "good": "ГРУЗЫ",
    "item": "ВИДЫ НАЧ и УДЕРЖ",
    "elem": "СТАТЬИ ДОХ и РАСХ",    
    "firm": "ОРГАНИЗАЦИИ",
    "pers": "СОТРУДНИКИ",
    "taxfl": "НАЛОГИ ФЛ",
    "taxip": "НАЛОГИ ИП",
    "trail": "ПОЛУПРИЦЕПЫ",
    "wage": "ЗАРПЛАТА",
    "auth_cas": "CAS",
    "auth_event": "EVENT",
    "auth_group": "GROUP",
    "auth_membership": "MEMBER",
    "auth_permission": "PERMIS",
    "auth_user": "USER",
}
#============================================================================
def SelectTable(lTbl):
    """
    Создание формы для выбора одной таблицы из выпадающего списка
    -------------------------------------------------------------------------
    @param lTbl [list] Список имён таблиц для создания выпадающего меню
    """
    #------------------------------------------------------------------------
    # Список пунктов выпадающего меню для выбора имени таблицы из списка
    lTabOpt = []
    #------------------------------------------------------------------------
    for im, snaim in lTbl:
        # Цикл по именам таблиц из списка
        #--------------------------------------------------------------------
        """
        Здесь:
        - im - имя таблицы  
        - snaim - сокращённое наименование таблицы
        - (_name = _value)  - словарь, отправляемый на сервер, после выбора
                              пользователем пункта меню. 
        - _name - ключ словаря
        - _value - значение словаря.

        При выборе из меню таблицы ТОПЛИВО,   {tbl_name: "fuel"}
        """
        #--------------------------------------------------------------------
        # Заполнение списка пунктов выпадающего меню
        # snaim - это значения, которые появляются в выпадающем меню        
        # _value - это значение словаря, который отправляется на сервер 
        # в виде 
        lTabOpt.append(OPTION(snaim, _value=im))
    #endfor
    #------------------------------------------------------------------------
    # Форма для выбора имени таблицы из выпадающего списка
    form = FORM(TABLE(
        TR(B('ИмяТаблицы:'),
           # Меню с именами таблиц
           SELECT(
               # Cписок пунктов выпадающего меню
               lTabOpt,
               #-------------------------------------------------------------
               # Ключ словаря выбранного пункта выпадающего меню
               _name = "tbl_name",
               )),
        TR('',
           # Кнопка "Готово"
           INPUT(
               # Тип кнопки - "Отправить"
               _type = 'submit',
               #-------------------------------------------------------------
               # Надпись на кнопке
               _value = 'Готово'
           ))
    ))
    #------------------------------------------------------------------------
    return form
#enddef
#============================================================================
def TruncTable(tbl_name):
    """
    Удаление из таблицы tbl_name всех записей. Установка начального значения
    поля id в 1.
    -------------------------------------------------------------------------
    @param tbl_name [str] Имя таблицы для чистки
    """
    #------------------------------------------------------------------------
    if db._dbname == "postgres" or db._dbname == "sqlite":
        # Текущая БД - PgSQL или SQLite
        #--------------------------------------------------------------------
        db[tbl_name].truncate('RESTART IDENTITY CASCADE')
        #--------------------------------------------------------------------
    elif db._dbname == "mysql":
        # Текущая БД - MySQL
        #--------------------------------------------------------------------
        # Удаление всех записей из таблицы БД, установка идентификатора
        # записи в 1
        db.executesql("SET FOREIGN_KEY_CHECKS=0;")
        db.executesql('TRUNCATE TABLE %s;' % tbl_name)
        db.executesql("SET FOREIGN_KEY_CHECKS=1;")
        #--------------------------------------------------------------------
    else:
        # Текущая БД - неизвестна
        #--------------------------------------------------------------------
        # Отправка info-сообщения в лог
        lg.info("Поддержка данной БД не реализована!")
        #--------------------------------------------------------------------
        # Аварийный выход из программы
        sys.exit(1)
    #endif
#enddef
#============================================================================
def carrier_snaim():
    """
    Выбор сокращённого наименования ПЕРЕВОЗЧИКА snaim по id записи из
    сессии session.firm1_id
    -------------------------------------------------------------------------
    @global session.firm1_id [int] Идентификатор записи из таблицы firm
                                    (ОРГАНИЗАЦИИ) в сессии
    """
    #------------------------------------------------------------------------
    ret = ""
    #------------------------------------------------------------------------
    if session.firm1_id:
        # Перевозчик был выбран пользователем
        #--------------------------------------------------------------------
        # Выбор из таблицы ОРГАНИЗАЦИИ объекта записи по её id
        row = db.firm[session.firm1_id]
        #--------------------------------------------------------------------
        if row:
            # Запись существует
            #----------------------------------------------------------------
            # Формирование строки состояния
            ret = "ПЕРЕВОЗЧИК-%s" % row.snaim
        #endif
    else:
        # Пользователем выбраны все ПЕРЕВОЗЧИКИ
        #--------------------------------------------------------------------
        # Формирование строки состояния
        ret = "ПЕРЕВОЗЧИК-ВСЕ"
    #endif
    #------------------------------------------------------------------------
    return ret
#enddef
#============================================================================
'''
def func_dx(row):
    """
    @param row [obj] Объект записи из таблицы oreis
    
    Объект Row является словарём словарей:
    <Row {'oreis': {'udat': datetime.date(2013, 6, 1), 'num': '44',
    'firm2_id': 1L, 'id': 1L, 'good_id': 4L, 'car_id': 1L, 'dtep': None, 
    'trail_id': 1L, 'dtbp': None, 'sgdout_id': 3L, 'pers_id': 1L, 
    'date': datetime.date(2013, 5, 7), 'dlits': None, 'dtbf': None,
    'firm1_id': 2L, 'dtef': None, 'route': '\xd0\x9c\xd0\xbe\xd1\x81
    \xd0\xba\xd0\xb2\xd0\xb0-\xd0\x9d.\xd0\x9d\xd0\xbe\xd0\xb2\xd0\xb3\xd0
    \xbe\xd1\x80\xd0\xbe\xd0\xb4', 'dlinas': None, 'dlinao': None, 
    'sgdin_id': 1L, 'firm3_id': 3L}}>
    
    Для определения id записи из таблицы oreis используем формулу:
    oreis_id = row["oreis"]["id"]
    -------------------------------------------------------------------------
    Функция обратного вызова для виртуального поля dx из таблицы oreis
    (см. определение таблицы oreis)
    """
    #------------------------------------------------------------------------
    # Вычисляемое поле с агрегатной функцией sum() для суммирования
    # доходов по колонке summa таблицы odxrx ДОХОДЫ И РАСХОДЫ
    sumdx = db.odxrx.summa.sum()
    #------------------------------------------------------------------------
    # Выборка из таблицы odxrx ДОХОДЫ И РАСХОДЫ записи с итоговой суммой
    # доходов dx по колонке summa
    r = db(db.odxrx.oreis_id==row["oreis"]["id"]) \
        (db.elem.id==db.odxrx.elem_id) \
        (db.elem.numst=="00").select(sumdx).first()
    #------------------------------------------------------------------------
    # Итоговая сумма доходов dx 
    return r[sumdx]
#enddef
#============================================================================
def func_rx(row):
    """
    @param row [obj] Объект записи из таблицы oreis
    
    Объект Row является словарём словарей:
    <Row {'oreis': {'udat': datetime.date(2013, 6, 1), 'num': '44',
    'firm2_id': 1L, 'id': 1L, 'good_id': 4L, 'car_id': 1L, 'dtep': None, 
    'trail_id': 1L, 'dtbp': None, 'sgdout_id': 3L, 'pers_id': 1L, 
    'date': datetime.date(2013, 5, 7), 'dlits': None, 'dtbf': None,
    'firm1_id': 2L, 'dtef': None, 'route': '\xd0\x9c\xd0\xbe\xd1\x81
    \xd0\xba\xd0\xb2\xd0\xb0-\xd0\x9d.\xd0\x9d\xd0\xbe\xd0\xb2\xd0\xb3\xd0
    \xbe\xd1\x80\xd0\xbe\xd0\xb4', 'dlinas': None, 'dlinao': None, 
    'sgdin_id': 1L, 'firm3_id': 3L}}>
    
    Для определения id записи из таблицы oreis используем формулу:
    oreis_id = row["oreis"]["id"]
    -------------------------------------------------------------------------
    Функция обратного вызова для виртуального поля rx из таблицы oreis
    (см. определение таблицы oreis)
    """
    #------------------------------------------------------------------------
    # Вычисляемое поле с агрегатной функцией sum() для суммирования
    # расходов по колонке summa таблицы odxrx ДОХОДЫ И РАСХОДЫ
    sumrx = db.odxrx.summa.sum()
    #------------------------------------------------------------------------
    # Выборка из таблицы odxrx ДОХОДЫ И РАСХОДЫ записи с итоговой суммой
    # расходов rx по колонке summa 
    r = db(db.odxrx.oreis_id==row["oreis"]["id"]) \
        (db.elem.id==db.odxrx.elem_id) \
        ((db.elem.numst=="50") | (db.elem.numst=="51")) \
        .select(sumrx).first()
    #------------------------------------------------------------------------
    # Итоговая сумма расходов rx 
    return r[sumrx]
#enddef
'''
#============================================================================
def func_dx(row):
    """
    @param row [obj] Объект записи из таблицы oreis
    
    Объект Row является словарём словарей:
    <Row {'oreis': {'udat': datetime.date(2013, 6, 1), 'num': '44',
    'firm2_id': 1L, 'id': 1L, 'good_id': 4L, 'car_id': 1L, 'dtep': None, 
    'trail_id': 1L, 'dtbp': None, 'sgdout_id': 3L, 'pers_id': 1L, 
    'date': datetime.date(2013, 5, 7), 'dlits': None, 'dtbf': None,
    'firm1_id': 2L, 'dtef': None, 'route': '\xd0\x9c\xd0\xbe\xd1\x81
    \xd0\xba\xd0\xb2\xd0\xb0-\xd0\x9d.\xd0\x9d\xd0\xbe\xd0\xb2\xd0\xb3\xd0
    \xbe\xd1\x80\xd0\xbe\xd0\xb4', 'dlinas': None, 'dlinao': None, 
    'sgdin_id': 1L, 'firm3_id': 3L}}>
    
    Для определения id записи из таблицы oreis используем формулу:
    oreis_id = row["oreis"]["id"]
    -------------------------------------------------------------------------
    Функция обратного вызова для вычисляемого поля dx из таблицы oreis
    (см. определение таблицы oreis)
    #------------------------------------------------------------------------
    r = db(db.odxrx.oreis_id==row["oreis"]["id"]) \
        (db.elem.id==db.odxrx.elem_id) \
        (db.elem.numst=="00").select(sumdx).first()
    """
    #------------------------------------------------------------------------
    # Итоговая сумма доходов dx 
    return None
#enddef
#============================================================================
def func_rx(row):
    return None
#enddef
#############################################################################